import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
const { resolve } = require('path');
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    extensions: ['.vue', '.ts', '.js', '.json', '.less', '.css'],
    alias: [
      {
        find: '@',
        replacement: resolve(__dirname, 'src')
      },
      {
        find: '@a',
        replacement: resolve(__dirname, 'src/assets')
      },
      {
        find: '@api',
        replacement: resolve(__dirname, 'src/api')
      },
      {
        find: '@pages',
        replacement: resolve(__dirname, 'src/pages')
      }
    ]
  },
  server: {
    open: true,
    port: 8080,
    host: '127.0.0.1',
    proxy: {
      '/api/': {
        target: 'http://123.60.137.241/',
        // target: 'http://127.0.0.1:8000/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
  base: 'http://static.admin.rainbowinpaper.cn/',
  build: {
    rollupOptions: {
      output: {
        chunkFileNames: 'js/[name]-[hash].js', // 引入文件名的名称
        entryFileNames: 'js/[name]-[hash].js', // 包的入口文件名称
        assetFileNames: '[ext]/[name]-[hash].[ext]', // 资源文件像 字体，图片等
      }
    }
  }
});
