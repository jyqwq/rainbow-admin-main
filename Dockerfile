FROM centos
MAINTAINER "JyQAQ"
RUN cd /etc/yum.repos.d/
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
 
RUN yum makecache
RUN yum update -y
RUN yum install -y nginx
WORKDIR /etc/nginx
COPY nginx.conf nginx.conf
WORKDIR /usr/share/nginx/html
COPY dist .
EXPOSE 80
CMD nginx -g "daemon off;"

#FROM nginx
#COPY nginx.conf /etc/nginx/nginx.conf
#COPY dist /usr/share/nginx/html
