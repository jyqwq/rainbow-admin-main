export interface StateType {
    currentUser: CurrentUser | { userInfo: undefined; routes: [] };
    option: any;
    iframeUrl: string;
    menuList: MenuProps[] | [];
    microRoutes: MenuProps[] | [];
    roleList: RoleProps[] | [];
    userList: UserProps[] | [];
    noButtonList: MenuProps[] | []; // 没有按钮的菜单列表 用于设置按钮父级菜单的下拉选择
    catalogList: MenuProps[] | []; // 目录列表 用于设置菜单的父级菜单的下拉选择
    navTabs: NavTabProps[] | [];
    loadedMicroApps: object;
    initRouter: boolean;
    keepAliveRef: any;
}
// 当前用户
export interface CurrentUser {
    userInfo: UserProps | undefined;
    routes: MenuProps[] | [];
}
export interface OpenTabProps {
    id?: number;
    name: string;
    path: string;
    tabType?: 'more' | 'one' | '';
    target: '_self' | '_blank';
    pageType: 'component' | 'iframe' | 'link' | 'micro' | '';
    link?: string;
    iframe?: string;
    query?: any;
    params?: any;
}
// 多标签页
export interface NavTabProps {
    id: number;
    name: string;
    path: string;
    fullPath: string;
    tabType?: 'more' | 'one' | '';
    active: boolean;
    query?: any;
    params?: any;
}
// 菜单
export interface MenuProps {
    id?: number;
    type: 'catalog' | 'menu' | 'button';
    name: string;
    path?: string;
    icon?: any;
    rank?: number | undefined;
    target: '_self' | '_blank';
    link?: string;
    iframe?: string;
    component?: string;
    pageType: 'component' | 'iframe' | 'link' | 'micro' | '';
    renderMenu?: boolean;
    permission?: string;
    parentId?: number;
    children?: MenuProps[] | [];
    deleted?: boolean;
    createTime?: number;
    active?: boolean;
    tabType?: 'more' | 'one' | '';
}
// 角色
export interface RoleProps {
    id?: number;
    name: string;
    permission: string;
    state?: boolean;
    permissionItems: string[] | permissionItemProps[] | [];
    deleted?: boolean;
    createTime?: number;
}
// 用户
export interface UserProps {
    id?: number;
    name: string;
    account: string;
    phone: string;
    password: string;
    roleId?: number | undefined;
    role?: RoleProps;
    state: boolean;
    createTime?: number;
    deleted?: boolean;
}
export interface permissionItemProps {
    id?: number;
    roleId: number;
    menuId: number;
    name: string;
    permission: string;
}
export interface initialProps {
    onShow?: Function;
    onHide?: Function;
}