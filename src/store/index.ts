import { createStore } from 'vuex';
import apis from '../api/request/auth';
import {StateType, NavTabProps} from '@/interface';
import { initRouter } from '@/utils/router';
import {message} from "ant-design-vue";

const initState: StateType = {
  currentUser: { userInfo: undefined, routes: [] },
  option: {},
  iframeUrl: '',
  menuList: [],
  microRoutes: [],
  noButtonList: [],
  catalogList: [],
  roleList: [],
  userList: [],
  navTabs: [],
  loadedMicroApps: {},
  initRouter: false,
  keepAliveRef: null
};
export default createStore({
  state: {
    ...initState
  },
  getters: {
    routesGetter(state) {
      return state.currentUser.routes;
    },
    navTabsGetter(state) {
      return state.navTabs;
    }
  },
  /**
   mutations方法书写的时候命名最后都加上Mutation，方便代码中辨认
   */
  mutations: {
    saveCurrentUserMutation(state, payload) {
      console.log('saveCurrentUserMutation');
      localStorage.setItem('currentUser', JSON.stringify(payload));
      initRouter(payload.routes);
      state.currentUser = payload;
    },
    saveMenuListMutation(state, payload) {
      const filterCatalog = (menu: any) => {
        return menu.map((item: any) => {
          if (item.type !== 'catalog') return null;
          if (item.children && item.children.length > 0) {
            item.children = filterCatalog(item.children);
          }
          return item
        }).filter((item: any) => item);
      };
      const filterButton = (menu: any) => {
        return menu.map((item: any) => {
          if (item.type === 'button') return null;
          if (item.children && item.children.length > 0) {
            item.children = filterButton(item.children);
          }
          return item
        }).filter((item: any) => item);
      };
      state.catalogList = filterCatalog(JSON.parse(JSON.stringify(payload)));
      state.noButtonList = filterButton(JSON.parse(JSON.stringify(payload)));
      state.menuList = payload;
    },
    saveMicroRoutesMutation(state, payload) {
      state.microRoutes = payload;
    },
    saveRoleListMutation(state, payload) {
      state.roleList = payload;
    },
    saveUserListMutation(state, payload) {
      state.userList = payload;
    },
    setIframeMutation(state, payload) {
      state.iframeUrl = payload;
    },
    setNavTabsMutation(state, payload) {
      state.navTabs = [...payload];
    },
    addNavTabsMutation(state, payload: NavTabProps) {
      if (!state.navTabs.length) {
        state.navTabs = [payload];
        return;
      }
      if (payload.tabType === 'one') {
        const index = state.navTabs.findIndex((item: any) => item.path === payload.path);
        if (index !== -1) {
          state.navTabs = [...state.navTabs.map((i,idx)=>({...i,active:idx===index}))];
        } else {
          state.navTabs = [...state.navTabs.map(i=>({...i,active:false})), payload];
        }
      } else {
        state.navTabs = [...state.navTabs.map(i=>({...i,active:false})), payload];
      }
    },
    delNavTabsMutation(state, payload) {
      if (state.navTabs.length === 1) return;
      const index = state.navTabs.findIndex((item: any, index: number) => {
        if (item.id === payload && item.active) {
          if (!index) {
            state.navTabs[index + 1].active = true;
          } else {
            state.navTabs[index - 1].active = true;
          }
        }
        return item.id === payload
      });
      state.navTabs = [...state.navTabs.filter((i,idx)=>idx!==index)];
    },
    clearNavTabsMutation(state) {
      state.navTabs = [{
        id: -1,
        name: '概览',
        path: '/',
        query: {},
        params: {},
        fullPath: '/',
        tabType: 'one',
        active: true
      }];
    },
    changeNavTabsMutation(state, payload) {
      state.navTabs = [...state.navTabs.map(i=>({...i,active:i.id===payload}))]
    },
    // refreshNavTabsMutation(state, payload) {
    //   state.navTabs = [...state.navTabs.map((i,idx)=>{
    //     if (i.id === payload) {
    //       return {...i,id: new Date().getTime()};
    //     }
    //     return i
    //   })];
    // },
    setLoadedMicroAppsMutation(state, payload) {
      state.loadedMicroApps = payload;
    },
    setInitRouterMutation(state, payload) {
      state.initRouter = payload;
    },
    setKeepAliveRefMutation(state, payload) {
      state.keepAliveRef = payload;
    }
  },
  /**
   actions方法书写的时候命名最后都加上Action，方便代码中辨认
   */
  actions: {
    async loginAction({ commit }, param) {
      try {
        const data: any = await apis.loginApi(param);
        commit('saveCurrentUserMutation', data || { userInfo: undefined, routes: [] });
        return true;
      } catch (error) {
        return false;
      }
    },
    async getUserMsgAction({ commit }) {
      try {
        const data: any = await apis.getUserMsgApi();
        commit('saveCurrentUserMutation', data || { userInfo: undefined, routes: [] });
        return true;
      } catch (error) {
        return false;
      }
    },
    async logoutAction({ commit }) {
      try {
        const data: any = await apis.logoutApi();
        commit('saveCurrentUserMutation', { userInfo: undefined, routes: [] });
        return true;
      } catch (error) {
        return false;
      }
    },
    async getMenuListAction({ commit }) {
      try {
        const data: any = await apis.getMenuListApi();
        commit('saveMenuListMutation', data || []);
        return true;
      } catch (error) {
        return false;
      }
    },
    async getRoleListAction({ commit }) {
      try {
        const data: any = await apis.getRoleListApi();
        commit('saveRoleListMutation', data || []);
        return true;
      } catch (error) {
        return false;
      }
    },
    async getUserListAction({ commit }) {
      try {
        const data: any = await apis.getUserListApi();
        commit('saveUserListMutation', data || []);
        return true;
      } catch (error) {
        return false;
      }
    },
    async editMenuAction({ commit, dispatch }, param) {
      try {
        const data: any = await apis.editMenuApi(param);
        if (data && !isNaN(data)) {
          message.success('编辑成功');
          dispatch('getMenuListAction');
          return true
        } else {
          return data
        }
      } catch (error) {
        return false;
      }
    },
    async editRoleAction({ commit, dispatch }, param) {
      try {
        const data: any = await apis.editRoleApi(param);
        if (data && !isNaN(data)) {
          message.success('编辑成功');
          dispatch('getRoleListAction');
          return true
        } else {
          return data
        }
      } catch (error) {
        return false;
      }
    },
    async editUserAction({ commit, dispatch }, param) {
      try {
        const data: any = await apis.editUserApi(param);
        if (data && !isNaN(data)) {
          message.success('编辑成功');
          dispatch('getUserListAction');
          return true
        } else {
          return data
        }
      } catch (error) {
        return false;
      }
    },
    async deleteMenuAction({ commit, dispatch }, param) {
      try {
        const data: any = await apis.deleteMenuApi(param);
        if (data) {
          message.success('删除成功');
          dispatch('getMenuListAction');
        } else {
          message.error('删除失败');
        }
        return true;
      } catch (error) {
        return false;
      }
    },
    async deleteRoleAction({ commit, dispatch }, param) {
      try {
        const data: any = await apis.deleteRoleApi(param);
        if (data) {
          message.success('删除成功');
          dispatch('getRoleListAction');
        } else {
          message.error('删除失败');
        }
        return true;
      } catch (error) {
        return false;
      }
    },
    async deleteUserAction({ commit, dispatch }, param) {
      try {
        const data: any = await apis.deleteUserApi(param);
        if (data) {
          message.success('删除成功');
          dispatch('getUserListAction');
        } else {
          message.error('删除失败');
        }
        return true;
      } catch (error) {
        return false;
      }
    }
  },
  modules: {}
});
