export default class Columns {
  static menuColumns = (h: any, $antIcons: any)=>{
    return [
      {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
        fixed: 'left',
        width: '200px'
      },
      {
        title: '类型',
        dataIndex: 'type',
        key: 'type',
        width: '100px',
        customRender: ({ text }: any) => {
          return text === 'catalog' ? '目录' : text === 'menu' ? '菜单' : '按钮';
        }
      },
      {
        title: '路径',
        dataIndex: 'path',
        key: 'path',
        width: '150px'
      },
      {
        title: '图标',
        dataIndex: 'icon',
        key: 'icon',
        width: '100px',
        customRender: ({ text }: any) => {
          return text && h(($antIcons as any)[text]);
        }
      },
      {
        title: '排序',
        dataIndex: 'rank',
        key: 'rank',
        width: '100px'
      },
      {
        title: '权限标识',
        dataIndex: 'permission',
        key: 'permission',
        width: '150px'
      },
      {
        title: '渲染菜单',
        dataIndex: 'renderMenu',
        key: 'renderMenu',
        width: '100px',
        customRender: ({ text }: any) => {
          return text ? '是' : '否';
        }
      },
      {
        title: '页面类型',
        dataIndex: 'pageType',
        key: 'pageType',
        width: '100px'
      },
      {
        title: '页面组件/链接',
        dataIndex: 'pageType',
        key: 'pageUrl',
        width: '200px',
        customRender: ({ text, record }: any) => {
          return record[text] || '';
        }
      },
      {
        title: '窗口类型',
        dataIndex: 'target',
        key: 'target',
        width: '100px',
        customRender: ({ text, record }: any) => {
          return record.type !== 'menu' ? '' : (text ==='_self' ? '当前窗口' : '新窗口');
        }
      },
      {
        title: '页签类型',
        dataIndex: 'tabType',
        key: 'tabType',
        width: '100px',
        customRender: ({ text }: any) => {
          return text === 'one' ? '单页签' : (text === 'more' ? '多页签' : '');
        }
      },
      {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
        width: '200px',
        customRender: ({ text }: any) => {
          return text ? new Date(text).toLocaleString().replaceAll('/','-') : '';
        }
      },
      {
        title: '操作',
        dataIndex: 'operation',
        key: 'operation',
        fixed: 'right',
        width: '206px'
      }
    ]
  }
  static roleColumns = ()=>{
    return [
      {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
        fixed: 'left',
        width: '200px'
      },
      {
        title: '权限标识',
        dataIndex: 'permission',
        key: 'permission',
        width: '150px'
      },
      {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        width: '100px',
        customRender: ({ text }: any) => {
          return text ? '启用' : '停用';
        }
      },
      {
        title: '权限项',
        dataIndex: 'permissionItems',
        key: 'permissionItems',
        customRender: ({ text }: any) => {
          if (text && text[0] === '*') return '全部权限';
          return text.map((item: any) => item.name).join(',');
        }
      },
      {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
        width: '200px',
        customRender: ({ text }: any) => {
          return text ? new Date(text).toLocaleString().replaceAll('/','-') : '';
        }
      },
      {
        title: '操作',
        dataIndex: 'operation',
        key: 'operation',
        width: '200px',
        fixed: 'right'
      }
    ]
  }

  static userColumns = ()=>{
    return [
      {
        title: '名称',
        dataIndex: 'name',
        key: 'name',
        width: '200px'
      },
      {
        title: '账号',
        dataIndex: 'account',
        key: 'account',
        width: '100px',
      },
      {
        title: '手机号',
        dataIndex: 'phone',
        key: 'phone',
        width: '150px'
      },
      {
        title: '密码',
        dataIndex: 'password',
        key: 'password',
        width: '200px'
      },
      {
        title: '角色',
        dataIndex: 'roleName',
        key: 'roleName',
        width: '150px'
      },
      {
        title: '状态',
        dataIndex: 'state',
        key: 'state',
        width: '100px',
        customRender: ({ text }: any) => {
          return text ? '启用' : '停用';
        }
      },
      {
        title: '创建时间',
        dataIndex: 'createTime',
        key: 'createTime',
        width: '200px',
        customRender: ({ text }: any) => {
          return text ? new Date(text).toLocaleString().replaceAll('/','-') : '';
        }
      },
      {
        title: '操作',
        dataIndex: 'operation',
        key: 'operation',
        fixed: 'right',
        width: '206px'
      }
    ]
  }
}