import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Login from '@/pages/Login/index.vue';
import Index from '@/pages/Index.vue';
import Views from '@/pages/Views.vue';
import NotFound from '@/pages/404.vue';
import NProgress from "nprogress";

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: '概览',
    component: Index,
    meta: {
      constant: true,
      id: -1,
      name: '概览'
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      constant: true
    }
  },
  {
    path: '/blank',
    name: 'blank',
    component: Views,
    meta: {
      constant: true,
      id: -2,
      name: 'blank'
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: NotFound,
    meta: {
      constant: true,
      id: -3,
      name: '404'
    }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

let initPage = true;

router.beforeResolve( (to, from, next) => {
  // 刷新页面时路由还没注册，所以要先等待注册路由，不做跳转
  if (!from.name && initPage) {
    sessionStorage.setItem('redirect_page', JSON.stringify({ path: to.path, query: to.query, params: to.params }));
    initPage = false;
    return next(false);
  }
  if (to.matched.length === 0) return next({name: '404'});
  if (['home', 'login', '404'].includes(to.name as string)) return next();
  NProgress.start();
  const { permission } = to.meta;
  let need_login = false;
  let currentUser = JSON.parse(localStorage.getItem('currentUser') || 'null')
  if (!currentUser && permission) {
    need_login = true;
  } else if (currentUser && permission) {
    let permissionItems = (currentUser as any).userInfo?.role?.permissionItems
    if (permissionItems.length === 1 && permissionItems[0] === '*') {
      need_login = false;
    } else {
      need_login = permissionItems.indexOf(permission) === -1;
    }
  }
  if (need_login) {
    return next({
      name: 'login',
      params: {
        redirect_page: to.name as string,
        ...to.params
      },
    })
  } else {
    return next()
  }
});

router.afterEach(() => {
  NProgress.done();
});

export default router;
