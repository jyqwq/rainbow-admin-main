import { rainbow_http_get, rainbow_http_post } from '../../utils/axiosAsync';
import { adminLoginType } from '../type/userTpye';
import {MenuProps} from "../../interface";

export default class apis {
    /**
     API方法书写的时候命名最后都加上Api，方便代码中辨认
     */
    // 登录请求
    static loginApi = (params: adminLoginType) => rainbow_http_post('/users/login', params);
    // 获取用户信息
    static getUserMsgApi = () => rainbow_http_get('/users/getUserMsg',{});
    // 退出登录
    static logoutApi = () => rainbow_http_get('/users/logout',{});
    // 获取菜单列表
    static getMenuListApi = () => rainbow_http_get('/auths/getMenuList',{});
    // 获取角色列表
    static getRoleListApi = () => rainbow_http_get('/auths/getRoleList',{});
    // 获取用户列表
    static getUserListApi = () => rainbow_http_get('/auths/getUserList',{});
    // 编辑菜单
    static editMenuApi = (param: MenuProps) => rainbow_http_post('/auths/editMenu',param);    
    // 编辑角色
    static editRoleApi = (param: MenuProps) => rainbow_http_post('/auths/editRole',param);
    // 编辑用户
    static editUserApi = (param: MenuProps) => rainbow_http_post('/auths/editUser',param);
    // 删除菜单
    static deleteMenuApi = (param: MenuProps) => rainbow_http_post('/auths/deleteMenu',param);
    // 删除角色
    static deleteRoleApi = (param: MenuProps) => rainbow_http_post('/auths/deleteRole',param);
    // 删除用户
    static deleteUserApi = (param: MenuProps) => rainbow_http_post('/auths/deleteUser',param);
}
