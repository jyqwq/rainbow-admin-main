import _ from 'lodash'
import store from '@/store'
import router from '@/router'
import {NavTabProps, OpenTabProps} from "@/interface";
import {createMicroApp, isMicroApp} from "@/micro/apps";

class Tabs {
  private compareTabs: NavTabProps[] = [];
  private tabs: NavTabProps[] = [];
  constructor() {
    !this.getLocalTabs() && this.setLocalTabs([])
    this.initTabs()
  }

  // 初始化Tab数组
  initTabs() {
    this.compareTabs = this.getLocalTabs() || []
    this.tabs = this.getLocalTabs() || []
    localStorage.getItem('currentUser') && store.commit('saveCurrentUserMutation', JSON.parse(<string>localStorage.getItem('currentUser')))
  }

  // 获取缓存中的页签组
  getLocalTabs() {
    return _.cloneDeep(JSON.parse(<string>localStorage.getItem('tabs')))
  }

  // 设置store页签组
  setLocalTabs(tabs: any[]) {
    store.commit('setNavTabsMutation', tabs)
  }
  
  // 打开新页签
  async openTab(el: OpenTabProps) {
    if (el.params && el.params.pathMatch) delete el.params.pathMatch
    let realRoute: any = el
    if (!el.pageType) {
      const _route = router.getRoutes().find((item: any) => item.path === el.path)
      if (_route) realRoute = {..._route.meta, ...el}
    }
    console.log(realRoute);
    const { target, pageType, link, iframe } = realRoute
    const path = realRoute.path[0] !== '/' ? `/${realRoute.path}` : realRoute.path
    const isMicro = isMicroApp(path)
    if (pageType === 'iframe') {
      store.commit('setIframeMutation', iframe);
    } else if (pageType === 'micro' || isMicro) {
      await createMicroApp(realRoute.path)
    } else if (pageType === 'link') {
      window.open(link, '_blank');
      return
    } else if (target === '_blank') {
      const route = router.resolve({path});
      window.open(route.href, '_blank');
      return
    }
    let isExist = false
    // 查找已打开的当前Tabs
    let openedTab = this.tabs.find((item:NavTabProps) => {
      if ((!el.tabType || el.tabType === 'one') && item.path === el.path) {
        item.active = true
        isExist = true
      } else if (el.tabType === 'more' && item.path === el.path && item.query === el.query && item.params === el.params) {
        item.active = true
        isExist = true
      } else {
        item.active = false
      }
      return (el.tabType === 'one' && item.path === el.path) || (el.tabType === 'more' && item.path === el.path && item.query === el.query && item.params === el.params)
    })
    // 如果存在已经打开的tab，取realRoute的path，跳转
    if (openedTab) realRoute = openedTab

    // 等待路由初始化完成
    let initTimer = null
    const toNext = () => {
      if (!store.state.initRouter) {
        initTimer = setTimeout(() => toNext(), 100)
        return
      }
      router
        .replace({ path: realRoute.path, query: realRoute.query || {}, params: realRoute.params || {} })
        .then(() => {
          // 如果不存在已经打开的tab，添加新的tab
          if (!isExist) {
            const menu = this.findWithPath(realRoute.path, realRoute.pageType === 'micro' && isMicroApp(path)) || {}
            console.log(menu);
            this.tabs.push({
              id: realRoute.id || new Date().getTime(),
              name: menu.name,
              path: menu.path,
              fullPath: menu.path,
              tabType: menu.meta?.tabType,
              active: true,
              query: realRoute.query || {},
              params: realRoute.params || {}
            })
          }
          this.setLocalTabs(this.tabs)
        })
        // .catch(() => {
        //   this.initTabs()
        // })
    }
    toNext()

  }
  
  // 切换页签
  changeTab(id: number) {
    this.tabs.forEach((item:NavTabProps) => {
      if (item.id === id) {
        item.active = true
        router
          .replace({ path: item.path, query: item.query || {}, params: item.params || {} })
          .then(() => this.setLocalTabs(this.tabs))
      }
    })
  } 
  
  // 刷新页签
  refreshTab(id: number, path: string) {
    // 动态清除keep-alive缓存
    const keepAlive = store.state.keepAliveRef.keepAlive;
    if (keepAlive && keepAlive._) {
      const vCache = keepAlive._.__v_cache;
      if (vCache.get(path)) vCache.delete(path)
    }
    store.state.keepAliveRef.pageRefresh();
  }
  
  removeTab(id: number, path: string) {
    if (this.tabs.length === 1) return;
    const keepAlive = store.state.keepAliveRef.keepAlive;
    if (keepAlive && keepAlive._) {
      const vCache = keepAlive._.__v_cache;
      if (vCache.get(path)) vCache.delete(path)
    }
    let tabs: NavTabProps[] = []
    let activeId = 0
    this.tabs.forEach((item:NavTabProps, index: number) => {
      if (item.id === id && item.active) {
        if (!index) {
          this.tabs[index + 1].active = true;
          activeId = this.tabs[index + 1].id
        } else {
          tabs[tabs.length - 1].active = true;
          activeId = tabs[tabs.length - 1].id
        }
      }
      if (item.id !== id) tabs.push(item)
    })
    this.tabs = tabs
    if (activeId) this.changeTab(activeId)
    else this.setLocalTabs(this.tabs)
  }
  
  // 通过path查找完整路由信息
  findWithPath(path: string, micro: boolean): any {
    if (micro) return store.state.microRoutes.find((route) => route.path === path)
    else return router.getRoutes().find((route) => route.path === path)
  }
}

let tabs = new Tabs()
export default tabs
