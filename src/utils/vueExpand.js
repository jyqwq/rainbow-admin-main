const fs = require('fs');
const path = require('path');

// 查找以 index- 开头的文件
const files = fs.readdirSync('./dist/js')
  .filter(file => file.startsWith('index-'))
  .map(file => path.join('./dist/js', file));

files.forEach(file => {
  // 读取文件内容
  let content = fs.readFileSync(file, { encoding: 'utf8' });

  let addStr = ''
  let startIndex = 0
  // 使用正则表达式查找代码插入位置
  try {
    const pattern1 = /,\s*[a-zA-Z]=[a-zA-Z]\.ctx;/g;
    const pattern2 = /const\s+[a-zA-Z]\s*=\s*new\s+Map\s*,\s*[a-zA-Z]\s*=\s*new\s+Set;/g;
    const match1 = pattern1.exec(content);
    const match2 = pattern2.exec(content);
    addStr = `${match1[0].split('=')[1].split('.')[0]}.__v_cache=${match2[0].split('=')[0].split(' ')[1]};`;
    startIndex = match2.index + match2[0].length;
  } catch (e) {
    console.log('文件正则匹配失败', e);
  }
  if (addStr) {
    console.log('即将写入的代码:', addStr);
    // 将代码插入到指定位置
    content = content.slice(0, startIndex) + addStr + content.slice(startIndex);
    // 将修改后的内容写回到文件
    fs.writeFileSync(file, content, { encoding: 'utf8' });
    console.log('file ', file, ' 写入成功');
  }
});
