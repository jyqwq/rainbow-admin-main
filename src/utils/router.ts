import router from '@/router';
import store from "@/store";
import { MenuProps } from "@/interface";

const loadView = import.meta.glob('../pages/**/*.vue');

function formatRouter(routes: MenuProps[], child: Boolean, parentPath: String=''): Array<any> {
  return routes.map((item: any) => {
    if (!item.renderMenu) return null;
    item.path[0] !== '/' && (item.path = `/${item.path}`);
    return {
      path: parentPath+item.path,
      name: item.name,
      component: item.pageType === 'micro' ? item.component : (item.component ? loadView[`../pages/${item.component}.vue`]: null),
      children: item.children ? formatRouter(item.children, true, item.path) : [],
      meta: {
        ...item
      }
    };
  }).filter((r) => r !== null);
}

function registerRouter(routes: any[]) {
  let microRoutes: any[] = [];
  if (!routes || !routes.length) return microRoutes;
  routes.forEach((item: any) => {
    if (item.meta.pageType === 'micro') {
      microRoutes.push(item);
    } else if (item.component && !router.hasRoute(item.name)) {
      router.addRoute(item);
    }
    if (item.children) {
      microRoutes = microRoutes.concat(registerRouter(item.children));
    }
  });
  return microRoutes;
}

export function initRouter(routes: Array<MenuProps>): void {
  resetRouter();
  const formattedRoutes = formatRouter(routes, false);
  const microRoutes = registerRouter(formattedRoutes);
  store.commit('saveMicroRoutesMutation', microRoutes);
  store.commit('setInitRouterMutation', true)
}

export function resetRouter(): void {
  router.getRoutes().forEach((route) => {
    const { name, meta } = route;
    if (name && !meta.constant) {
      router.hasRoute(name) && router.removeRoute(name);
    }
  });
  store.commit('setInitRouterMutation', false)
}