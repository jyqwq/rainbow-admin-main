import {onActivated, onDeactivated} from "vue";
import {initialProps} from "@/interface";
import NProgress from "nprogress";

export function initialization(config: initialProps | undefined) {
  const {onShow, onHide} = config || {};
  onActivated(() => {
    if (sessionStorage.getItem('refreshing')) {
      sessionStorage.removeItem('refreshing');
      NProgress.done();
    }
    onShow && onShow();
  });
  onDeactivated(() => {
    onHide && onHide();
  });
}