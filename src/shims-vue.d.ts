declare module '*.vue' {
  import { DefineComponent } from 'vue';
  const component: DefineComponent<{}, {}, any>;
  export default component;
}
declare module "nprogress" {
  class NProgress {
    /** 开始加载 */
    static start: Function;
    /** 结束加载 */
    static done: Function;
  }
  export default NProgress;
}
declare module '*.svg';
declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.gif';
declare module '*.bmp';
declare module '*.tiff';
