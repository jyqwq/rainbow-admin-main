import { loadMicroApp } from 'qiankun'
import store from '@/store'

export const microAppList: any = [
  {
    id: 'ManageMicroApp',
    name: 'ManageMicroApp',
    entry: process.env.NODE_ENV === 'production' ? '/system/' : '//localhost:10200',
    container: '#ManageMicroApp',
    activeRule: '/manage'
  },
  {
    id: 'FormDesignerMicroApp',
    name: 'FormDesignerMicroApp',
    entry: process.env.NODE_ENV === 'production' ? '/designer/' : '//localhost:8088',
    container: '#ManageMicroApp',
    activeRule: '/formDesigner'
  }
];

/**
 * @description 查找当前页签是否是微应用下的页面
 */
export function isMicroApp(path: string): boolean {
  return !!microAppList.some((item: any) => {
    return path.startsWith(item.activeRule)
  })
}
/**
 * @description 查找当前页签是否是微应用下的页面，并返回对应微应用配置项
 */
export function findMicroAppByPath(path: string) {
  return microAppList.find((item: any) => {
    let activeRule = item.activeRule
    return path.startsWith(activeRule)
  })
}
/**
 * @description 创建微应用
 */
export function createMicroApp(path: string) {
  return new Promise((resolve, reject) => {
    const loadedMicroApps: any = { ...store.state.loadedMicroApps } // 已手动挂载的微应用对象
    if (!isMicroApp(path)) {
      // 非微应用直接跳转
      resolve('notMicroApp')
      return
    }

    // 微应用跳转处理
    /**
     * @description 1.判断是否已手动加载，是则直接跳转，否则先手动挂载，再跳转
     */
    const microAppResult = findMicroAppByPath(path) // 是否是微应用的跳转
    if (Object.prototype.hasOwnProperty.call(loadedMicroApps, microAppResult.name)) {
      console.log('打开已挂载的微应用==>', store.state.loadedMicroApps)
      resolve('hasLoaded')
      return
    }
    try {
      loadedMicroApps[microAppResult.name] = loadMicroApp(microAppResult) // 加载微应用
      store.commit('setLoadedMicroAppsMutation', loadedMicroApps)
      console.log('挂载后的已挂载的微应用==>', store.state.loadedMicroApps)
      resolve('loadMicroAppSuccess')
    } catch (err) {
      reject(err)
      console.log(err)
    }
  })
}
