import {createApp} from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import tabs from "@/utils/tabs";
import 'ant-design-vue/dist/reset.css';
import '@/assets/css/reset.css';
import '@/assets/css/common.less';
import Antd from 'ant-design-vue';
// import startQiankun from './micro';

import * as antIcons from '@ant-design/icons-vue'

sessionStorage.removeItem('redirect_page')

const app = createApp(App)

app.directive('permission', {
    mounted(el, binding) {
        const current_permission = store.state.currentUser.userInfo?.role?.permissionItems || []
        if (current_permission && current_permission[0] === '*') return;
        else {
            // @ts-ignore
            if (current_permission && current_permission.indexOf(binding.value) === -1) {
                el.parentNode && el.parentNode.removeChild(el);
            }
        }
    }
})

// 配置全局对象
app.config.globalProperties.$antIcons = antIcons
app.config.globalProperties.$tabs = tabs
Object.keys(antIcons).forEach((key) => {
    (app as any).component(key, (antIcons as any)[key])
})

app.use(router)

app.use(store).use(Antd).mount('#main-app')
// startQiankun({sandbox: {experimentalStyleIsolation: true}})
